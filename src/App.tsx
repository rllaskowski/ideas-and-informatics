import React from 'react';
import { Navigate, RouteObject, useRoutes } from 'react-router-dom';
import RegisterView from './views/register';
import LoginView from './views/login';
import Layout from './components/layout';
import BoardView from './views/board';
import ProfileView from './views/profile';
import { ChakraProvider } from '@chakra-ui/react';
import { useIsAuthenticated } from './state/auth';
import CoursesView from './views/courses';

const routes: RouteObject[] = [
    {
        path: '',
        element: <Layout />,
        children: [
            {
                path: '/',
                element: <BoardView />,
            },
            {
                path: '/register',
                element: <RegisterView />,
            },
            {
                path: '/login',
                element: <LoginView />,
            },
            {
                path: '/profile',
                element: <ProfileView />,
            },
            {
                path: '/courses',
                element: <CoursesView />,
            },
            {
                path: '*',
                element: <Navigate to={'/'} replace />,
            },
        ],
    },
];

const App: React.FC = () => {
    const appRoutes = useRoutes(routes);

    return <ChakraProvider>{appRoutes}</ChakraProvider>;
};

export default App;
