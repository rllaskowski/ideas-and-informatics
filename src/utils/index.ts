export const generateRandomElements = <T extends unknown>(array: T[], count: number): T[] => {
    const result: T[] = [];
    const indexes: number[] = [];

    while (result.length < count) {
        const randomIndex = Math.floor(Math.random() * array.length);

        if (!indexes.includes(randomIndex)) {
            indexes.push(randomIndex);
            result.push(array[randomIndex]);
        }
    }

    return result;
};
