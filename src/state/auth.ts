import { createGlobalState, useLocalStorage } from 'react-use';
import { useCallback } from 'react';
import { useToast } from '@chakra-ui/react';
import { User, users } from '../data/users';

export const useAuthUser = createGlobalState<User | undefined>(users[0]);

export const useLogout = () => {
    const [, setAuthUser] = useAuthUser();
    const toast = useToast();
    return useCallback(() => {
        setAuthUser(undefined);
        toast({
            position: 'bottom-left',
            isClosable: true,
            status: 'success',
            title: 'Poprawnie wylogowano',
        });
    }, [setAuthUser, toast]);
};

export const useLogin = () => {
    const [, setAuthUser] = useAuthUser();
    const toast = useToast();
    return useCallback(
        (user: User) => {
            toast({
                position: 'bottom-left',
                isClosable: true,
                status: 'success',
                title: 'Poprawnie zalogowano',
            });
            setAuthUser(user);
        },
        [setAuthUser, toast]
    );
};

export const useIsAuthenticated = () => {
    const [user] = useAuthUser();
    return !!user;
};
