import React from 'react';
import { Box, Button, FormControl, FormErrorMessage, Input } from '@chakra-ui/react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useLogin } from '../../state/auth';
import { users } from '../../data/users';

type LoginInput = {
    email: string;
    password: string;
};

const LoginForm: React.FC = () => {
    const login = useLogin();
    const {
        register,
        setError,
        handleSubmit,
        formState: { errors },
        setValue,
    } = useForm<LoginInput>();

    const onSubmit: SubmitHandler<LoginInput> = ({ email, password }) => {
        const user = users.find((user) => user.email === email && user.password === password);
        if (user) {
            login(user);
        } else {
            setError('email', {
                type: 'custom',
                message: 'Błędny e-mail lub hasło',
            });
            setValue('password', '');
        }
    };
    return (
        <Box mt={10}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <FormControl isInvalid={!!errors.email} mb={3}>
                    <Input
                        placeholder={'Email'}
                        type={'email'}
                        {...register('email', { required: true })}
                    />
                    {errors.email && <FormErrorMessage>{errors.email.message}</FormErrorMessage>}
                </FormControl>
                <FormControl isInvalid={!!errors.password} mb={3}>
                    <Input
                        type={'password'}
                        placeholder={'Hasło'}
                        {...register('password', { required: true })}
                    />
                </FormControl>
                <Button type={'submit'}>Zaloguj się</Button>
            </form>
        </Box>
    );
};

export default LoginForm;
