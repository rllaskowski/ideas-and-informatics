import React from 'react';
import { useIsAuthenticated } from '../../state/auth';
import { Navigate } from 'react-router-dom';
import { Box, Container, Heading } from '@chakra-ui/react';
import LoginForm from './login-form';

const LoginView: React.FC = () => {
    const isAuthenticated = useIsAuthenticated();

    if (isAuthenticated) {
        return <Navigate to={'/'} replace />;
    }

    return (
        <Container>
            <Box py={20}>
                <Heading>Zaloguj się</Heading>
                <LoginForm />
            </Box>
        </Container>
    );
};

export default LoginView;
