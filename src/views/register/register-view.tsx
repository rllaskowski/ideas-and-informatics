import React from 'react';
import { useIsAuthenticated } from '../../state/auth';
import { Navigate } from 'react-router-dom';
import RegisterForm from './register-form';
import { Box, Container, Heading } from '@chakra-ui/react';

const RegisterView: React.FC = () => {
    const isAuthenticated = useIsAuthenticated();

    if (isAuthenticated) {
        return <Navigate to={'/'} replace />;
    }

    return (
        <Container>
            <Box py={20}>
                <Heading>Zarejestruj się</Heading>
                <RegisterForm />
            </Box>
        </Container>
    );
};

export default RegisterView;
