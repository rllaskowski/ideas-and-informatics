import React from 'react';
import { Box, Button, FormControl, FormErrorMessage, Input, useToast } from '@chakra-ui/react';
import { SubmitHandler, useForm } from 'react-hook-form';

import { useNavigate } from 'react-router-dom';

type RegisterInput = {
    email: string;
    password: string;
    passwordRepeat: string;
};

const RegisterForm: React.FC = () => {
    const toast = useToast();
    const navigate = useNavigate();
    const {
        register,
        setError,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<RegisterInput>();

    const onSubmit: SubmitHandler<RegisterInput> = ({ email, password, passwordRepeat }) => {
        let error = false;
        if (password !== passwordRepeat) {
            setError('passwordRepeat', { type: 'custom', message: 'Hasła nie są takie same' });
            error = true;
        }
        if (password.length < 8) {
            setError('password', {
                type: 'custom',
                message: 'Hasło musi mieć conajmniej 8 znaków',
            });
            error = true;
        }
        if (!error) {
            reset();
            toast({
                position: 'bottom-left',
                isClosable: true,
                status: 'success',
                title: 'Poprawnie zarejestowano, sprawdź pocztę w celu weryfikacji konta',
            });
            navigate('/login');
        }
    };

    return (
        <Box mt={10}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <FormControl isInvalid={!!errors.email} mb={3}>
                    <Input
                        placeholder={'Email'}
                        type={'email'}
                        {...register('email', { required: true })}
                    />
                    {errors.email && <FormErrorMessage>{errors.email.message}</FormErrorMessage>}
                </FormControl>
                <FormControl isInvalid={!!errors.password} mb={3}>
                    <Input
                        type={'password'}
                        placeholder={'Hasło'}
                        {...register('password', { required: true })}
                    />
                    {errors.password && (
                        <FormErrorMessage>{errors.password.message}</FormErrorMessage>
                    )}
                </FormControl>
                <FormControl isInvalid={!!errors.passwordRepeat} mb={3}>
                    <Input
                        type={'password'}
                        placeholder={'Powtórz hasło'}
                        {...register('passwordRepeat', { required: true })}
                    />
                    {errors.passwordRepeat && (
                        <FormErrorMessage>{errors.passwordRepeat.message}</FormErrorMessage>
                    )}
                </FormControl>

                <Button type={'submit'}>Zarejestruj się</Button>
            </form>
        </Box>
    );
};

export default RegisterForm;
