import React from 'react';
import { useIsAuthenticated } from '../../state/auth';
import { Navigate } from 'react-router-dom';
import SearchBar from './search-bar';
import { Box, Container } from '@chakra-ui/react';
import OfferList from './offer-list';

const BoardView: React.FC = () => {
    const isAuthenticated = useIsAuthenticated();
    if (!isAuthenticated) {
        return <Navigate to={'/login'} replace />;
    }

    return (
        <Box>
            <SearchBar />
            <Container py={5} maxW="2xl">
                <OfferList />
            </Container>
        </Box>
    );
};

export default BoardView;
