import React, { useCallback } from 'react';
import {
    Box,
    Button,
    Center,
    FormControl,
    FormLabel,
    HStack,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    RangeSlider,
    RangeSliderFilledTrack,
    RangeSliderMark,
    RangeSliderThumb,
    RangeSliderTrack,
    Switch,
    Text,
    useDisclosure,
} from '@chakra-ui/react';
import { BsSearch } from 'react-icons/bs';
import { Item } from 'chakra-ui-autocomplete';
import { locations, positions, seniorities } from '../../data/offers';
import { useSearchParams } from 'react-router-dom';
import CategorySelect from '../../components/category-select';

const locationItems: Item[] = locations.map((location) => ({
    label: location.name,
    value: location.id,
}));

const positionItems: Item[] = positions.map((position) => ({
    label: position.name,
    value: position.id,
}));

const seniorityItems: Item[] = seniorities.map((seniority) => ({
    label: seniority.name,
    value: seniority.id,
}));

const SalarySlider: React.FC = () => {
    const [sliderValue, setSliderValue] = React.useState([0, 100]);

    return (
        <FormControl>
            <FormLabel>Wybierz widełki miesięcznego wynagrodzenia</FormLabel>

            <RangeSlider
                aria-label={['min', 'max']}
                value={sliderValue}
                onChange={setSliderValue}
                mt={4}
            >
                <RangeSliderMark value={sliderValue[0]} textAlign="center" mt={1} fontSize="sm">
                    {sliderValue[0] * 500} PLN
                </RangeSliderMark>
                <RangeSliderMark
                    value={sliderValue[1]}
                    textAlign="center"
                    mt={-6}
                    ml={`${4 * (-20 + Math.floor((100 - sliderValue[1]) * 0.2))}px`}
                    fontSize="sm"
                >
                    {sliderValue[1] * 500} PLN
                </RangeSliderMark>
                <RangeSliderTrack>
                    <RangeSliderFilledTrack />
                </RangeSliderTrack>
                <RangeSliderTrack>
                    <RangeSliderFilledTrack />
                </RangeSliderTrack>
                <RangeSliderThumb index={0} />
                <RangeSliderThumb index={1} />
            </RangeSlider>
        </FormControl>
    );
};

const AIAssistantSwitch: React.FC = () => {
    return (
        <FormControl display="flex" alignItems="center" mb={6}>
            <FormLabel htmlFor="ai-assistant" mb="0">
                Korzystaj z propozycji asystenta kariery
            </FormLabel>
            <Switch id="ai-assistant" defaultChecked={true} />
        </FormControl>
    );
};

const SearchBar: React.FC = () => {
    const { isOpen, onClose, onOpen } = useDisclosure();
    const [_, setSearchParams] = useSearchParams();

    const onSearch = useCallback(() => {
        setSearchParams({ search: Math.random().toString(36).substring(7) });
        onClose();
    }, [setSearchParams, onClose]);

    return (
        <Box w={'100%'} py={8} bg={'gray.50'} borderBottomWidth={'1px'}>
            <Center>
                <HStack
                    bg={'white'}
                    px={40}
                    py={3}
                    boxShadow={'xs'}
                    rounded={'md'}
                    color={'gray.500'}
                    _hover={{
                        boxShadow: 'outline',
                        cursor: 'pointer',
                    }}
                    onClick={onOpen}
                >
                    <BsSearch />
                    <Text> Wyszukaj oferty</Text>
                </HStack>
            </Center>
            <Modal blockScrollOnMount={false} isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>
                        <HStack>
                            <BsSearch />
                            <Text>Wyszukaj oferty</Text>
                        </HStack>
                    </ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <AIAssistantSwitch />
                        <CategorySelect
                            items={locationItems}
                            placeholder={'Wpisz lokalizację'}
                            label={'Wybierz preferowane lokalizacje'}
                        />
                        <CategorySelect
                            items={positionItems}
                            placeholder={'Wpisz stanowisko'}
                            label={'Wybierz preferowane stanowiska'}
                        />
                        <CategorySelect
                            items={seniorityItems}
                            placeholder={'Wpisz poziom doświadczenia'}
                            label={'Wybierz preferowane poziomy doświadczenia'}
                        />
                        <SalarySlider />
                    </ModalBody>

                    <ModalFooter>
                        <Button colorScheme="blue" mr={3} onClick={onSearch}>
                            Wyszukaj
                        </Button>
                        <Button variant="ghost" onClick={onClose}>
                            Zamknij
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </Box>
    );
};

export default SearchBar;
