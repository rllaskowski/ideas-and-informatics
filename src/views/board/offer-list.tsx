import React, { useMemo } from 'react';
import {
    Box,
    Button,
    Card,
    CardBody,
    CardFooter,
    Heading,
    Stack,
    Text,
    Badge,
    HStack,
    Tag,
    Spacer,
} from '@chakra-ui/react';
import { generateOfferList, JobOffer } from '../../data/offers';
import { LoremIpsum } from 'react-lorem-ipsum';

import { useSearchParams } from 'react-router-dom';

type OfferProps = {
    offer: JobOffer;
};
const OfferCard: React.FC<OfferProps> = ({ offer }) => {
    return (
        <Card direction={{ base: 'column', sm: 'row' }} overflow="hidden" variant="outline" mb={2}>
            <Stack>
                <CardBody>
                    <HStack>
                        <Heading size="md">{offer.title}</Heading>
                        <Spacer />
                        <Tag
                            variant="solid"
                            colorScheme="teal"
                        >{`${offer.yearSalaryMin} - ${offer.yearSalaryMax}zł`}</Tag>
                    </HStack>
                    <Text py="2">
                        <LoremIpsum p={1} avgSentencesPerParagraph={4} />
                    </Text>

                    <HStack>
                        {offer.tags.map((tag) => (
                            <Badge key={tag.id}>{tag.name}</Badge>
                        ))}
                    </HStack>
                    <Badge variant="subtle" colorScheme="green">
                        {offer.location.name}
                    </Badge>
                </CardBody>

                <CardFooter>
                    <Button variant="solid" colorScheme="blue">
                        Aplikuj
                    </Button>
                </CardFooter>
            </Stack>
        </Card>
    );
};

const OfferList: React.FC = () => {
    const [searchParams] = useSearchParams();
    const search = searchParams.get('search');

    const offerList = useMemo(generateOfferList, [search]);

    return (
        <Box>
            {offerList.map((offer) => (
                <OfferCard offer={offer} key={offer.id} />
            ))}
        </Box>
    );
};

export default OfferList;
