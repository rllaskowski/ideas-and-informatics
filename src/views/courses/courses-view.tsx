import React from 'react';
import {
    Button,
    Card,
    CardBody,
    CardFooter,
    Heading,
    Stack,
    Image,
    Text,
    Wrap,
    Divider,
    ButtonGroup,
    Badge,
    HStack,
    Tag,
    Box,
    Checkbox,
    Grid,
    GridItem,
    Switch,
} from '@chakra-ui/react';
import { authors, Course, courses } from '../../data/courses';
import { skills } from '../../data/users';

type CourseCardProps = {
    course: Course;
};

const CourseCard: React.FC<CourseCardProps> = ({ course }) => {
    return (
        <Card maxW="sm" variant="outline">
            <CardBody position={'relative'}>
                <Tag position={'absolute'} colorScheme={'purple'} top={5} right={5}>
                    {course.site}
                </Tag>
                <Image src={course.image} alt="Course image" borderRadius="lg" />
                <Stack mt="6">
                    <Text fontSize="sm" color="gray.500">
                        {course.author}
                    </Text>
                    <Heading size="md">{course.title}</Heading>

                    <Text>{course.description}</Text>
                    <Text color="blue.600" fontSize="2xl">
                        {course.price === 0 ? 'Darmowy' : `${course.price} PLN`}
                    </Text>
                </Stack>
                <HStack mt={2}>
                    {course.tags.map((tag) => (
                        <Badge id={tag.id} colorScheme="green">
                            {tag.name}
                        </Badge>
                    ))}
                </HStack>
            </CardBody>
            <Divider />
            <CardFooter>
                {course.progress === 0 ? (
                    <ButtonGroup spacing="2">
                        <Button variant="solid" colorScheme="blue">
                            Rozpocznij kurs
                        </Button>
                        <Button variant="ghost" colorScheme="blue">
                            Obserwuj
                        </Button>
                    </ButtonGroup>
                ) : (
                    <Button variant="solid" colorScheme="blue">
                        Kontynuuj kurs
                    </Button>
                )}
            </CardFooter>
        </Card>
    );
};

const SideBarFilters: React.FC = () => {
    const isChecked = () => Math.random() > 0.7;

    return (
        <Box>
            <Heading size="md" mb={8} mt={2}>
                Filtry
            </Heading>

            <HStack mb={8}>
                <Heading size={'sm'} mr={2}>
                    Korzystaj z propozycji asystenta
                </Heading>
                <Switch defaultChecked={true} />
            </HStack>
            <HStack mb={8}>
                <Heading size={'sm'} mr={2}>
                    Tylko rozpoczęte
                </Heading>
                <Switch />
            </HStack>

            <Heading size={'sm'} mb={2}>
                Umiejętności
            </Heading>
            <Stack>
                {skills.map((skill) => (
                    <Checkbox
                        key={skill}
                        size="lg"
                        defaultChecked={isChecked()}
                        colorScheme={'blue'}
                    >
                        {skill}
                    </Checkbox>
                ))}
            </Stack>
            <Heading size={'sm'} mb={2} mt={8}>
                Autorzy
            </Heading>
            <Stack>
                {authors.map((author) => (
                    <Checkbox
                        key={author}
                        size={'lg'}
                        defaultChecked={isChecked()}
                        colorScheme={'blue'}
                    >
                        {author}
                    </Checkbox>
                ))}
            </Stack>
        </Box>
    );
};

const CoursesList: React.FC = () => {
    return (
        <Wrap spacingY={6} spacingX={12} justify={'left'}>
            {courses
                .sort((a, b) => b.progress - a.progress)
                .map((course) => (
                    <CourseCard key={course.id} course={course} />
                ))}
        </Wrap>
    );
};

const CoursesView: React.FC = () => {
    return (
        <Grid templateAreas={`"nav main"`} py={10} px={20} gridTemplateColumns={'350px 1fr'}>
            <GridItem area={'nav'}>
                <SideBarFilters />
            </GridItem>
            <GridItem area={'main'}>
                <Heading mb={8}>20 wyników wyszukiwania</Heading>
                <CoursesList />
            </GridItem>
        </Grid>
    );
};

export default CoursesView;
