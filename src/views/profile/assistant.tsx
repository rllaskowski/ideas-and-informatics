import React, { useState } from 'react';
import {
    Box,
    Flex,
    FormControl,
    FormLabel,
    Heading,
    Image,
    Select,
    Slider,
    SliderFilledTrack,
    SliderMark,
    SliderThumb,
    SliderTrack,
    Switch,
    Tooltip,
} from '@chakra-ui/react';
import roadmap from '../../static/img/roadmap.png';
import { getRandomAssistantConfig, roads } from '../../data/assistant';
import CategorySelect from '../../components/category-select';
import { Item } from 'chakra-ui-autocomplete';
import { positions } from '../../data/offers';

const positionItems: Item[] = positions.map((position) => ({
    label: position.name,
    value: position.id,
}));

const DetailLevel: React.FC = () => {
    const [sliderValue, setSliderValue] = useState(5);
    const [showTooltip, setShowTooltip] = useState(false);

    return (
        <FormControl>
            <FormLabel>Wybierz poziom szczegółowości roadmapy</FormLabel>
            <Slider
                id="slider"
                defaultValue={5}
                min={0}
                max={100}
                colorScheme="blue"
                onChange={(v) => setSliderValue(v)}
                onMouseEnter={() => setShowTooltip(true)}
                onMouseLeave={() => setShowTooltip(false)}
            >
                <SliderMark value={0} mt="1" fontSize="sm">
                    Brak szczegółów
                </SliderMark>

                <SliderMark value={100} mt="1" ml="-135px" fontSize="sm">
                    Wszystkie szczegóły
                </SliderMark>
                <SliderTrack>
                    <SliderFilledTrack />
                </SliderTrack>
                <Tooltip hasArrow placement="top" isOpen={showTooltip} label={`${sliderValue}%`}>
                    <SliderThumb />
                </Tooltip>
            </Slider>
        </FormControl>
    );
};

const Config: React.FC = () => {
    const config = getRandomAssistantConfig();

    return (
        <Box w={'500px'}>
            <FormControl mb={6}>
                <FormLabel mb={4}> Twoja aktualna ścieżka</FormLabel>

                <Select defaultValue={config.road}>
                    {roads.map((road) => (
                        <option key={road} value={road}>
                            {road}
                        </option>
                    ))}
                </Select>
            </FormControl>
            <CategorySelect
                items={positionItems}
                placeholder={'Wpisz stanowisko'}
                label={'Wybierz preferowane stanowiska'}
                defaultItems={config.positions.map((position) => ({
                    label: position.name,
                    value: position.id,
                }))}
            />
            <FormControl display="flex" alignItems="center" mb={6}>
                <FormLabel htmlFor="course-notifications" mb="0">
                    Powiadamiaj o nowych kursach
                </FormLabel>
                <Switch id="course-notifications" defaultChecked={true} />
            </FormControl>

            <FormControl display="flex" alignItems="center" mb={6}>
                <FormLabel htmlFor="offers-notifications" mb="0">
                    Powiadamiaj o nowych ofertach
                </FormLabel>
                <Switch id="offers-notifications" defaultChecked={true} />
            </FormControl>
            <DetailLevel />
        </Box>
    );
};

const Assistant: React.FC = () => {
    return (
        <Flex justifyContent={'center'} alignItems={'start'}>
            <Box mr={8}>
                <Heading size={'lg'} mb={8}>
                    Dostostuj ustawienia
                </Heading>
                <Config />
            </Box>
            <Image
                src={roadmap}
                rounded={'md'}
                border={'1px'}
                borderColor={'gray.200'}
                maxW={'800px'}
                height={'auto'}
                p={2}
            />
        </Flex>
    );
};

export default Assistant;
