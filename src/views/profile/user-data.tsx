import React, { useState } from 'react';
import {
    Card,
    CardBody,
    Container,
    FormControl,
    FormLabel,
    Heading,
    Input,
    Select,
    Text,
    Box,
    HStack,
    Spacer,
    IconButton,
    Modal,
    ModalBody,
    ModalFooter,
    Button,
    useDisclosure,
    ModalOverlay,
    ModalContent,
    CardHeader,
} from '@chakra-ui/react';
import { countries, Experience, skills, User } from '../../data/users';
import { useAuthUser } from '../../state/auth';
import { MdAdd, MdEdit } from 'react-icons/md';
import { companies, positions } from '../../data/offers';
import { Item } from 'chakra-ui-autocomplete';
import CategorySelect from '../../components/category-select';

type UserProps = {
    user: User;
};

const UserInformation: React.FC<UserProps> = ({ user }) => {
    return (
        <Card mb={4} variant={'outline'}>
            <CardBody>
                <Heading size="md" mb={4}>
                    Informacje
                </Heading>
                <FormControl mb={2}>
                    <FormLabel>Imię i nazwisko</FormLabel>
                    <Input placeholder="Wpisz imię i nazwisko" defaultValue={user.fullName} />
                </FormControl>
                <FormControl>
                    <FormLabel>Kraj zamieszkania</FormLabel>
                    <Select placeholder="Wybierz kraj" defaultValue={user.country}>
                        {countries.map((country) => (
                            <option key={country} value={country}>
                                {country}
                            </option>
                        ))}
                    </Select>
                </FormControl>
            </CardBody>
        </Card>
    );
};

type ExperienceEditorProps = {
    experience: Experience;
};

const ExperienceEditor: React.FC<ExperienceEditorProps> = ({ experience }) => {
    return (
        <>
            <FormControl mb={2}>
                <FormLabel>Stanowisko</FormLabel>
                <Select placeholder="Wybierz stanowisko" defaultValue={experience.position.id}>
                    {positions.map((position) => (
                        <option key={position.id} value={position.id}>
                            {position.name}
                        </option>
                    ))}
                </Select>
            </FormControl>
            <FormControl mb={2}>
                <FormLabel>Pracodawca</FormLabel>
                <Select placeholder="Wybierz pracodawcę" defaultValue={experience.company.id}>
                    {companies.map((company) => (
                        <option key={company.id} value={company.id}>
                            {company.name}
                        </option>
                    ))}
                </Select>
            </FormControl>
        </>
    );
};

type ExperienceModalProps = {
    experience: Experience;
    render: (onOpen: () => void) => React.ReactNode;
};

const ExperienceModal: React.FC<ExperienceModalProps> = ({ experience, render }) => {
    const { isOpen, onOpen, onClose } = useDisclosure();

    return (
        <>
            {render(onOpen)}
            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay>
                    <ModalContent>
                        <ModalBody>
                            <ExperienceEditor experience={experience} />
                        </ModalBody>
                        <ModalFooter>
                            <Button colorScheme="blue" mr={3} onClick={onClose}>
                                Zapisz
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </ModalOverlay>
            </Modal>
        </>
    );
};

type ExperienceProps = {
    experience: Experience;
};

const ExperienceCard: React.FC<ExperienceProps> = ({ experience }) => {
    const { position, company, dateFrom, dateTo, description } = experience;
    const formatDate = (date: Date) => {
        return `${date.toLocaleString('pl', { month: 'short' })} ${date.getFullYear()}`;
    };
    return (
        <Box borderBottomWidth={1} mb={2} py={2}>
            <HStack>
                <Heading size={'sm'}>{`${position.name}`}</Heading>
                <Spacer />
                <ExperienceModal
                    experience={experience}
                    render={(onOpen) => (
                        <IconButton aria-label={'Edytuj'} onClick={onOpen} icon={<MdEdit />} />
                    )}
                />
            </HStack>

            <Text>{company.name}</Text>
            <Text fontSize={'sm'} color={'gray.600'}>{`${formatDate(dateFrom)} - ${formatDate(
                dateTo
            )}`}</Text>
            <Text mt={2}>{description}</Text>
        </Box>
    );
};

const UserExperience: React.FC<UserProps> = ({ user }) => {
    return (
        <Card mb={4} variant={'outline'}>
            <CardBody>
                <HStack>
                    <Heading size="md" mb={4}>
                        Doświadczenie
                    </Heading>
                    <Spacer />
                    <IconButton aria-label={''} icon={<MdAdd />} />
                </HStack>

                {user.experience.map((exp) => (
                    <ExperienceCard experience={exp} key={exp.id} />
                ))}
            </CardBody>
        </Card>
    );
};

const skillOptions: Item[] = skills.map((skill) => ({
    value: skill,
    label: skill,
}));

const UserSkills: React.FC<UserProps> = ({ user }) => {
    const userSkills = user.skills.map((skill) => ({
        value: skill,
        label: skill,
    }));

    return (
        <Card variant={'outline'}>
            <CardBody>
                <Heading size="md" mb={4}>
                    Umiejętności
                </Heading>
                <CategorySelect
                    defaultItems={userSkills}
                    items={skillOptions}
                    label={'Twoje umiejętności'}
                    placeholder={'Wybierz swoje umiejętności'}
                />
            </CardBody>
        </Card>
    );
};

const UserData = () => {
    const [authUser] = useAuthUser();

    return (
        <Container>
            <UserInformation user={authUser as User} />
            <UserExperience user={authUser as User} />
            <UserSkills user={authUser as User} />
        </Container>
    );
};

export default UserData;
