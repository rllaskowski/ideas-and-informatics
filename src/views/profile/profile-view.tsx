import React from 'react';
import { Box, Tab, TabList, TabPanel, TabPanels, Tabs } from '@chakra-ui/react';
import UserData from './user-data';
import Assistant from './assistant';
import { useIsAuthenticated } from '../../state/auth';
import { Navigate } from 'react-router-dom';

const ProfileView: React.FC = () => {
    const isAuthenticated = useIsAuthenticated();

    if (!isAuthenticated) {
        return <Navigate to={'/login'} replace />;
    }

    return (
        <Box>
            <Tabs>
                <TabList>
                    <Tab>Asystent kariery</Tab>
                    <Tab>Moje dane</Tab>
                </TabList>

                <TabPanels>
                    <TabPanel>
                        <Assistant />
                    </TabPanel>
                    <TabPanel>
                        <UserData />
                    </TabPanel>
                </TabPanels>
            </Tabs>
        </Box>
    );
};

export default ProfileView;
