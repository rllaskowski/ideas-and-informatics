import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
    Box,
    Button,
    CloseButton,
    Collapse,
    HStack,
    Input,
    Spacer,
    useDisclosure,
} from '@chakra-ui/react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { getFirstReply, getReply } from '../data/assistant';
import { FaRobot } from 'react-icons/fa';

type Message = {
    isUser: boolean;
    text: string;
};

type Input = {
    message: string;
};

const AssistantChat: React.FC = () => {
    const { isOpen, onClose, onOpen } = useDisclosure({ defaultIsOpen: false });

    const [messages, setMessages] = useState<Message[]>([
        {
            isUser: false,
            text: getFirstReply(),
        },
    ]);
    const messagesBox = useRef<HTMLDivElement>(null);
    const { handleSubmit, register, reset } = useForm<Input>();

    const onSubmit: SubmitHandler<Input> = useCallback(
        ({ message }) => {
            if (message.length === 0) {
                return;
            }
            setMessages((messages) => [...messages, { isUser: true, text: message }]);
            reset();
        },
        [reset]
    );

    useEffect(() => {
        if (messages.length === 0) {
            return;
        }
        if (messagesBox.current) {
            messagesBox.current.scrollTop = messagesBox.current.scrollHeight;
        }

        const lastMessage = messages[messages.length - 1];
        if (lastMessage.isUser) {
            setTimeout(() => {
                setMessages((messages) => [...messages, { isUser: false, text: getReply() }]);
            }, 1000);
        }
    }, [messages]);

    return (
        <>
            <Button
                onClick={onOpen}
                position={'fixed'}
                bottom={10}
                right={10}
                colorScheme={'blue'}
                leftIcon={<FaRobot />}
            >
                Czat z asystentem
            </Button>
            <Collapse in={isOpen} style={{ zIndex: 10 }} delay={2}>
                <Box
                    bottom={10}
                    right={10}
                    padding={10}
                    position={'fixed'}
                    boxShadow="base"
                    p="4"
                    pt={'2'}
                    rounded="md"
                    bg="gray.50"
                >
                    <HStack mb={2}>
                        <Spacer />
                        <CloseButton onClick={onClose} />
                    </HStack>
                    <Box
                        ref={messagesBox}
                        bg={'white'}
                        rounded={'md'}
                        mb={2}
                        p={2}
                        pt={2}
                        boxShadow={'xs'}
                        h={300}
                        overflow={'scroll'}
                    >
                        {messages.map((message, index) => (
                            <HStack
                                key={index}
                                justifyContent={message.isUser ? 'flex-end' : 'flex-start'}
                            >
                                <Box
                                    bg={message.isUser ? 'blue.200' : 'gray.200'}
                                    color={message.isUser ? 'white' : 'black'}
                                    p={2}
                                    mb={1}
                                    rounded="lg"
                                >
                                    {message.text}
                                </Box>
                            </HStack>
                        ))}
                    </Box>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <HStack>
                            <Input
                                {...register('message')}
                                placeholder="Wpisz wiadomość"
                                bg={'white'}
                            />{' '}
                            <Button type={'submit'}>Wyślij</Button>
                        </HStack>
                    </form>
                </Box>
            </Collapse>
        </>
    );
};

export default AssistantChat;
