import React from 'react';
import TopBar from './top-bar';
import { Box, Container } from '@chakra-ui/react';
import { Outlet } from 'react-router-dom';
import { useIsAuthenticated } from '../state/auth';
import AssistantChat from './assistant-chat';

const Layout: React.FC = () => {
    const isAuthenticated = useIsAuthenticated();

    return (
        <Box>
            <TopBar />
            <Outlet />
            {isAuthenticated && <AssistantChat />}
        </Box>
    );
};

export default Layout;
