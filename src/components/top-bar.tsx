import React, { createElement } from 'react';
import { HStack, Icon as ChIcon, Link, Spacer, Text } from '@chakra-ui/react';
import { useIsAuthenticated, useLogout } from '../state/auth';
import { Link as RouterLink } from 'react-router-dom';
import { ReactComponent as MdsLogout } from '@material-symbols/svg-400/rounded/logout.svg';
import { ReactComponent as MdsProfile } from '@material-symbols/svg-400/rounded/account_circle-fill.svg';
import { FaSearch } from 'react-icons/fa';
import { BsBookFill } from 'react-icons/bs';

// @ts-ignore
export const Icon = (props) => {
    return (
        <ChIcon
            {...props}
            as={(x: any) =>
                createElement(props.as, {
                    ...x,
                    fill: 'currentColor',
                    viewBox: '0 0 48 48',
                })
            }
        />
    );
};

const TopBar: React.FC = () => {
    const isAuthenticated = useIsAuthenticated();

    const logout = useLogout();
    return (
        <HStack px={{ base: 4, md: 8 }} py={5} bg="blue.700" color="white" gap={8}>
            <Text fontWeight={700}>AIC Assistant</Text>
            <Spacer />
            {isAuthenticated && (
                <>
                    <Link as={RouterLink} to={'/courses'}>
                        Kursy
                        <ChIcon fontSize={'20px'} ml={2}>
                            <BsBookFill />
                        </ChIcon>
                    </Link>
                    <Link as={RouterLink} to="/">
                        Szukaj ofert
                        <ChIcon fontSize={'20px'} ml={2}>
                            <FaSearch />
                        </ChIcon>
                    </Link>
                    <Link as={RouterLink} to={'/profile'}>
                        Profil
                        <Icon
                            as={MdsProfile}
                            fontSize="20px"
                            ml={2}
                            verticalAlign="middle"
                            color={'white'}
                        />
                    </Link>
                    <Link onClick={logout}>
                        Wyloguj się
                        <Icon
                            as={MdsLogout}
                            fontSize="20px"
                            ml={2}
                            verticalAlign="middle"
                            color={'white'}
                        />
                    </Link>
                </>
            )}
            {!isAuthenticated && (
                <>
                    <Link as={RouterLink} to={'/login'}>
                        Zaloguj się
                    </Link>
                    <Link as={RouterLink} to={'/register'}>
                        Zarejestruj się
                    </Link>
                </>
            )}
        </HStack>
    );
};

export default TopBar;
