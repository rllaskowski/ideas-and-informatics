import React from 'react';
import { CUIAutoComplete, Item } from 'chakra-ui-autocomplete';

type CategorySelectProps = {
    items: Item[];
    defaultItems?: Item[];
    label: string;
    placeholder: string;
};

const CategorySelect: React.FC<CategorySelectProps> = ({
    items,
    label,
    placeholder,
    defaultItems,
}) => {
    const [selectedLocationItems, setSelectedLocationItems] = React.useState<Item[]>(
        defaultItems || []
    );

    const handleSelectedItemsChange = (selectedItems?: Item[]) => {
        if (selectedItems) {
            setSelectedLocationItems(selectedItems);
        }
    };

    const filter = (items: Item[], filterValue: string) => {
        return items.filter((item) => item.label.toLowerCase().includes(filterValue.toLowerCase()));
    };

    return (
        <CUIAutoComplete
            disableCreateItem
            label={label}
            placeholder={placeholder}
            items={items}
            optionFilterFunc={filter}
            selectedItems={selectedLocationItems}
            onSelectedItemsChange={(changes) => handleSelectedItemsChange(changes.selectedItems)}
        />
    );
};

export default CategorySelect;
