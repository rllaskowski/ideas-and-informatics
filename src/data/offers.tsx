import { generateRandomElements } from '../utils';

export type Tag = {
    id: string;
    name: string;
};

export type Location = {
    id: string;
    name: string;
};

export type Company = {
    id: string;
    name: string;
};

export type Position = {
    id: string;
    name: string;
};

export type Seniority = {
    id: string;
    name: string;
};

export type JobOffer = {
    id: string;
    title: string;
    company: Company;
    position: Position;
    seniority: Seniority;
    location: Location;
    isRemote: boolean;
    yearSalaryMin: number;
    yearSalaryMax: number;
    tags: Tag[];
};

export const companies: Company[] = [
    {
        id: '1',
        name: 'Google',
    },
    {
        id: '2',
        name: 'Facebook',
    },
    {
        id: '3',
        name: 'Amazon',
    },
    {
        id: '4',
        name: 'Apple',
    },
];

export const positions: Position[] = [
    {
        id: '1',
        name: 'Frontend Developer',
    },
    {
        id: '2',
        name: 'Backend Developer',
    },
    {
        id: '3',
        name: 'Fullstack Developer',
    },
    {
        id: '4',
        name: 'DevOps',
    },
    {
        id: '5',
        name: 'QA',
    },
];

export const seniorities: Seniority[] = [
    {
        id: '1',
        name: 'Junior',
    },
    {
        id: '2',
        name: 'Mid',
    },
    {
        id: '3',
        name: 'Senior',
    },
];

export const locations: Location[] = [
    {
        id: '0',
        name: 'Zdalnie',
    },
    {
        id: '1',
        name: 'Warszawa',
    },
    {
        id: '2',
        name: 'Kraków',
    },
    {
        id: '3',
        name: 'Wrocław',
    },
    {
        id: '4',
        name: 'Poznań',
    },
    {
        id: '5',
        name: 'Gdańsk',
    },
    {
        id: '6',
        name: 'Katowice',
    },
];

export const tags: Tag[] = [
    {
        id: '1',
        name: 'JavaScript',
    },
    {
        id: '2',
        name: 'TypeScript',
    },
    {
        id: '3',
        name: 'React',
    },
    {
        id: '4',
        name: 'Angular',
    },
    {
        id: '5',
        name: 'Vue',
    },
    { id: '6', name: 'Python' },
    { id: '7', name: 'Java' },
    { id: '8', name: 'C#' },
    { id: '9', name: 'C++' },
    { id: '10', name: 'C' },
    { id: '11', name: 'PHP' },
    { id: '12', name: 'Ruby' },
    { id: '13', name: 'Go' },
];

const generateOffer = (id: number): JobOffer => {
    const company = companies[Math.floor(Math.random() * companies.length)];
    const position = positions[Math.floor(Math.random() * positions.length)];
    const seniority = seniorities[Math.floor(Math.random() * seniorities.length)];
    const location = locations[Math.floor(Math.random() * locations.length)];
    const _tags = generateRandomElements(tags, Math.floor(Math.random() * 3) + 1);
    const yearSalaryMin = Math.floor(Math.random() * 100) * 1000 + 1000;
    const yearSalaryMax = yearSalaryMin + Math.floor(Math.random() * 10) * 1000 + 1000;

    return {
        id: id.toString(),
        company,
        position,
        seniority,
        location,
        isRemote: location.id === '0',
        yearSalaryMin,
        yearSalaryMax,
        tags: _tags,
        title: `${company.name} - ${position.name} (${seniority.name})`,
    };
};

export const generateOfferList = (): JobOffer[] =>
    Array.from({ length: 20 }, (_, id) => generateOffer(id));
