import { Tag, tags } from './offers';
import { generateRandomElements } from '../utils';
import { loremIpsum } from 'react-lorem-ipsum';

export type Course = {
    id: string;
    title: string;
    description: string;
    tags: Tag[];
    rate: number;
    reviewsCount: number;
    price: number;
    author: string;
    site: string;
    image: string;
    progress: number;
};

export const sites: string[] = [
    'Udemy',
    'Coursera',
    'Pluralsight',
    'Udacity',
    'Skillshare',
    'LinkedIn Learning',
    'Codecademy',
];

export const titles: string[] = [
    'Python dla każdego',
    'Python od podstaw',
    'Finanse i rachunkowość',
    'Mikroekonomia',
    'Makroekonomia',
    'Finanse osobiste',
    'Programowanie w C++',
    'Programowanie w C',
    'Programowanie w Java',
    'Machine Learning',
    'Deep Learning',
    'Kurs JavaScript',
    'Kurs HTML',
    'Jak zarobić na giełdzie',
    'Co ugotować na obiad',
    'Jak zrobić ciasto',
    'Jak zrobić sałatkę',
    'Gdzie i jak wybrać się na wakacje',
    'Co robią milionerzy',
    'Jak zostać milionerem',
    'Jak zostać programistą',
    'Jak zostać informatykiem',
    'Jak zostać kucharzem',
    'Fryzjerstwo - moja i Twoja pasja',
];

export const authors: string[] = [
    'John Doe',
    'Yale University',
    'Stanford University',
    'Harvard University',
    'MIT',
    'University of Oxford',
    'University of Cambridge',
    'University of California, Berkeley',
    'University of Chicago',
];

const getRandomCourse = (id: number): Course => {
    const isFree = Math.random() > 0.7;
    const isStarted = Math.random() > 0.7;

    return {
        id: id.toString(),
        title: titles[Math.floor(Math.random() * titles.length)],
        description: loremIpsum({
            avgWordsPerSentence: 10,
            avgSentencesPerParagraph: 2,
            startWithLoremIpsum: false,
            random: false,
        }).join(' '),
        tags: generateRandomElements(tags, Math.floor(Math.random() * 5) + 1),
        rate: Math.floor(Math.random() * 5) + 1,
        reviewsCount: Math.floor(Math.random() * 1000),
        price: isFree ? 0 : Math.floor(Math.random() * 100) + 100,
        author: authors[Math.floor(Math.random() * authors.length)],
        site: sites[Math.floor(Math.random() * sites.length)],
        image: `https://picsum.photos/seed/${id}/400/300`,
        progress: isStarted ? Math.floor(Math.random() * 100) : 0,
    };
};

export const courses: Course[] = Array.from({ length: 20 }, (_, i) => getRandomCourse(i));
