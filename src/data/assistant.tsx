import { Position, positions } from './offers';
import { generateRandomElements } from '../utils';

export const replies: string[] = [
    'Jasne, to proste!',
    'Właśnie to robię!',
    'Świetnie Ci idzie!',
    'Może podejdź do tego inaczej?',
    'Zastanów się nad tym jeszcze raz.',
    'Nie jest to takie proste.',
    'Coś tu nie gra.',
];

export const roads = ['Kucharz', 'Informatyk', 'Programista', 'Piosenkarz'];

export const getFirstReply = () => {
    return 'Hej, jak mogę Ci pomóc?';
};

export const getReply = (): string => {
    return replies[Math.floor(Math.random() * replies.length)];
};

type AssistantConfig = {
    road: string;
    positions: Position[];
};

export const getRandomAssistantConfig = (): AssistantConfig => {
    return {
        road: roads[Math.floor(Math.random() * roads.length)],
        positions: generateRandomElements(positions, 3),
    };
};
