import { companies, Company, Position, positions, seniorities, Seniority } from './offers';
import { loremIpsum } from 'react-lorem-ipsum';
import { generateRandomElements } from '../utils';

export const countries: string[] = [
    'Polska',
    'Niemcy',
    'Francja',
    'Wielka Brytania',
    'USA',
    'Kanada',
    'Australia',
    'Chiny',
    'Japonia',
    'Indie',
];

export const skills: string[] = [
    'JavaScript',
    'TypeScript',
    'Python',
    'Azure',
    'GCP',
    'Docker',
    'Kubernetes',
    'React',
    'Angular',
    'Gotowanie',
    'Księgowość',
    'Finanse',
    'Ekonomia',
    'Programowanie',
    'Informatyka',
    'Fryzjerstwo',
];

export type Experience = {
    id: string;
    dateFrom: Date;
    dateTo: Date;
    description: string;
    company: Company;
    position: Position;
    seniority: Seniority;
};

export type User = {
    id: string;
    email: string;
    password: string;
    fullName: string;
    country: string;
    experience: Experience[];
    skills: string[];
};

const getRandomExperience = (id: number): Experience => {
    const dateFrom = new Date(2010 + Math.random() * 10, Math.random() * 12, Math.random() * 28);
    const dateTo = new Date(dateFrom.getTime() + Math.random() * 10000000000);

    return {
        id: id.toString(),
        dateFrom,
        dateTo,
        description: loremIpsum({
            avgWordsPerSentence: 10,
            avgSentencesPerParagraph: 2,
            startWithLoremIpsum: false,
            random: true,
        }).join(''),
        company: companies[Math.floor(Math.random() * companies.length)],
        position: positions[Math.floor(Math.random() * positions.length)],
        seniority: seniorities[Math.floor(Math.random() * seniorities.length)],
    };
};

export const users: User[] = [
    {
        id: '1',
        email: 'iai@mimuw.edu.pl',
        password: 'iai',
        fullName: 'Ideas and informatics',
        country: countries[Math.floor(Math.random() * countries.length)],
        experience: Array.from({ length: Math.floor(Math.random() * 2) + 2 }, (_, id) =>
            getRandomExperience(id)
        ),
        skills: generateRandomElements(skills, Math.floor(Math.random() * 3) + 3),
    },
];
